# Low-Tech Recherche et Enseignement (LowTRE) : répertoire de projets étudiants
*Vous pouvez trouver sur ce framagit des rapports étudiants liés aux Low-Tech répartis par années*
Ces rapports sont **documentés sur une [instance Zotero](https://www.zotero.org/groups/2831596/projets_etudiants_low_tech/items/XXMFW6GC/item-details)**

## Comment mettre en ligne mon rapport ?
Tout d'abord, vous pouvez parfaitement stocker votre rapport sur une autre plateforme que celle-ci (en évitant les solutions issues des GAFAM). Si c'est le cas, passez directement à l'étape 4.
1. [Créer un compte framagit](https://framagit.org/users/sign_up) OU connectez vous via un compte existant github, gitlab ou gitbucket.
2. Demandez accès à ce projet
3. Déposez votre fichier dans le dossier de l'année correspondant
4. Créez une référence Zotero associée sur l'[instance dédiée](https://www.zotero.org/groups/2831596/projets_etudiants_low_tech/items/XXMFW6GC/item-details), et indiquez les métadonnées (auteurs, années, thématiques, ...). Vous pouvez retrouver un guide ainsi que des propositions de page de garde en suivant [ce lien](https://pad.lescommuns.org/Accueil_LowTech_Enseignement#3-Une-biblioth%C3%A8que-des-projets-%C3%A9tudiants-r%C3%A9alis%C3%A9s).


## Quelques guides

- [ ] [Créer](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) ou [mettre en ligne](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) des fichiers
- [ ] [En utilisant la ligne de commande](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) 

## Licence
Les rapports sont de préférence en licence libre (CC-BY 4.0 ou CC-BY-SA 4.0), mais chaque auteur reste libre de la licence qu'il ou elle applique.

## Forum LowTRE 
Lien vers le [Forum LowTRE](https://forum-lowtre-ecosesa.univ-grenoble-alpes.fr/) et le [sujet dédié](https://forum-lowtre-ecosesa.univ-grenoble-alpes.fr/t/bibliotheque-des-projets-etudiants-low-tech/276)
