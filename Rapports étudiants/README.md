# Rapports étudiants Low-Tech
(*voir le [template](https://framagit.org/SachaHq/lowtre/-/blob/main/Rapports%20%C3%A9tudiants/template.md) pour compléter ce README*)

## 2020
### Marmite norvégienne
### Biodigesteur

## 2021
### Solar Oven
  - Éléa Fortin, Laura Mallet, Jeanne Berbari, Lucas Chatelais, Tijani Baali, Xilin Lei, Yongyu Pan
  - P013 Solar Oven
  - Type de contenu étude Technique
  - Type de projet : projet étudiant 
  - Thématiques: Energie, Alimentation
  - Discipline Ingénierie
  - Grenoble 
  - Grenoble INP - ENSE3
  - Responsable du projet et contact: Martial Balland
  - Licence: None
  - Résumé: Face à l’urgence des enjeux climatiques, il est nécessaire de se tourner vers des technologies sobres, techno et éco-responsables. Dans le but de contenir le réchauffement climatique, nous devons diviser nos émissions individuelles de GES (gaz à effet de serre) par 4 d’ici 2050 et atteindre l’objectif des 2 tCO2eq par habitant par an. La cuisson de nos aliments fait partie des activités domestiques quotidiennes émissives et non durables, par la consommation de gaz ou d'électricité. Le four solaire low-tech offre une nouvelle perspective puisqu’il est accessible à tous et répond favorablement aux critères de sobriété, de durabilité et d’écologie. Son utilisation n’émet pas de GES et permet un cuisson lente des aliments, un atout pour notre santé. Un four solaire est un caisson isolé thermiquement avec une vitre au-dessus, entouré de quatre réflecteurs. Ces réflecteurs vont réfléchir et concentrer les rayons lumineux sur la vitre, ce qui permet de créer de la chaleur dans le caisson, contenant la nourriture à cuire ou à réchauffer.
Le projet a pour but la conception ainsi que l'évaluation de l'efficacité énergétique du four. Des études théoriques ont été menées sur les propriétés de différents matériaux, l'ensoleillement et plus généralement sur le fonctionnement du four solaire. Nous avons
ensuite construit, à l’aide d’un tutoriel, le four solaire avec des matériaux de récupération.
Nous avons modélisé le four et comparé les températures mesurées expérimentalement avec celles obtenues par les simulations. Le four solaire est fonctionnel pour tout type de nourriture.
L’énergie du four utilisée étant intermittente et les conditions météorologiques étant défavorables, une seule expérience de mesure a pu être menée. Toutefois, les résultats qui en ressortent sont en accord avec la modélisation. L'étude du four vise à crédibiliser les technologies low-tech pour qu’elles puissent susciter un réel intérêt et se développer à grande échelle dans les années à venir. De plus, la réalisation d’un tel projet permet de démocratiser la low-tech auprès d’un public qui sera capable d’agir pour limiter le réchauffement climatique.

### Beyond the refrigerator : Caractérisation d'un système de stockage de fruits et légumes Low-tech (Garde-Manger)
  - Auteurices: Nawel Bentarcha José Ricardo Da Costa Pauline Maisonneuve Armand Marquet Hugo Matry Duvan Sacristan Théo Tirilly
  - Rapport_projet_p012_garde_manger.pdf
  - Type de contenu étude Technique
  - Type de projet : projet étudiant 
  - Thématiques: Energie, Alimentation
  - Discipline Ingénierie
  - Grenoble 
  - Grenoble INP - ENSE3
  - Responsable du projet et contact: Kévin Loeslé
  - Licence: None

### Marmite norvégienne: Adaptation à la restauration collective suite à un rapport sur les performances de la MN
  - Auteurices: Elsa Emond - Nathan Pitillion - Charlotte Zhuang - Enzo Fagnoni - Léo Dufour - Lionel Staub
  - P011 - Low-tech fireless cooker - Rapport.pdf
  - Type de contenu (par exemple Tutoriel Construction, étude Technique, Application d’une solution technique existante, Création d’une organisation, d’un etablissement Low-Tech, Mise en relation des Low Tech avec différents acteurs, Ateliers, Formations, Autre)
  - Type de projet : Tutoriel construction - Modèle de MN adapté; Normes de restauration collective; Template pour diffuser les projets
  - Thématiques: Energie, Alimentation, matériaux
  - Discipline Ingénierie
  - Grenoble 
  - Grenoble INP - ENSE3
  - Responsable du projet et contact: Sacha Hodencq sacha.hodencq@univ-grenoble-alpes.fr
  - Licence: CC-BY-SA 4.0

### Low-Tech Biogas producer
  - Auteurices: Timé BARRIERE, Paul BEGHIN, Mohamed EL FAKKAK, François FONTANILLES, Lucas LOUARN, Marion MAUGET
  - Nom du fichier: P015-LBTP_rapport.pdf
  - Type de projet : projet étudiant 
  - Thématiques Energie, déchets
  - Discipline: Ingénierie
  - Grenoble 
  - Grenoble INP - ENSE3
  - Mots clefs 
  - Responsable du projet et contact 
  - Licence: None

## 2022
### Projet Charbonnier
  - Auteurs: Jean Adam, Octave Delaye, Axel Di Campo, Louis Château, Zoé Jobard, Marc Lejeune
  - Type de projet : projet étudiant 
  - Thématiques Eau, Energie, Matériaux
  - Discipline: Ingénierie
  - Grenoble 
  - Grenoble INP - PISTE
  - Mots clefs: charbon, traitement de l'eau, pyrolyse
  - Responsable du projet et contact: Culture Ailleurs: cultureailleurs@yahoo.fr
  - Licence: CC-BY-SA
  - Résumé: trois documents: une introduction sur le projet et un état de l'art sur le charbon; un rapport sur le traitement de l'eau via du charbon, et un rapport sur le four pyrolyse pour le génération de charbon.

### LUTH : Évaluation technique et appropriation par l’usage de low-tech installées à la Maison des Familles de Grenoble
  - Auteurs: Challet Marceau - de Goër Baptiste - Guillemaud Gabin - Jouët-Pastré Rémi
  - Licence: CC-BY-SA
  - Grenoble INP - PISTE

### Transition alimentaire et Low-Tech : Le tube à stérilisation solaire 
  - Laure FERREIRA - Stevan HUBERT - Aurélien LEBEGUE - Marie MARCHESI - Agathe MENAGE
  - Mots clefs:  TRANSITION ALIMENTAIRE – STÉRILISATION – ÉNERGIE SOLAIRE
  - Licence: CC-BY-SA 4.0
  - Résumé : La transition alimentaire, vue comme la transformation de notre système alimentaire pour le rendre durable et plus inclusif, est un enjeu essentiel de la transition écologique sur lequel il est nécessaire de s’attarder. Principalement, car cette transition vise une production de nourriture en quantité suffisante tout en limitant le réchauffement planétaire et nos impacts sur l’environnement. Les outils et les méthodes qui tentent d’y répondre sont nombreux, et ce rapport présente l’une des possibilités : la stérilisation par tubes solaires. « Est-ce que l’utilisation de la stérilisation solaire est pertinente au regard de la transition alimentaire ? », voici la problématique à laquelle ce rapport tente de répondre. Notre but est alors d’aborder cette problématique sous 4 prismes différents : Économique, social, technique et environnemental. Nous avons alors utilisé des outils comme des entretiens sociologiques, de la recherche bibliographique, des tests de stérilisation avec les tubes solaires, de la modélisation sous Matlab ou encore de l’Analyse de Cycle de Vie sous Simapro afin d’avoir une vision globale de cette solution. Les différentes études que nous avons menées ont mis en lumière la nécessité d’une solution sobre et facile d’utilisation ainsi que certaines voies d’amélioration dont pourrait profiter la stérilisation solaire. Les études techniques nous ont permis de mettre au point un abaque permettant d’estimer le temps nécessaire pour réaliser de la stérilisation solaire. L’Analyse de Cycle de Vie nous a permis d’observer les leviers à activer
pour rendre plus responsable, environnementalement parlant, l’utilisation des tubes solaires. Enfin, l’analyse économique nous atteste que l’utilisation pour un public de particulier est tout à fait indiquée, mais que pour un usage plus professionnel, une étude plus approfondie est nécessaire. Finalement, ces différentes études nous confirment que la stérilisation par tubes solaires est pertinente dans le contexte de la transition alimentaire et que le temps investi dans ce sujet est opportun.

## 2023
### Bilan option Welow et de la formation
  - Centrale Nantes
### Numérique Low-tech
  - Auteurices: Julie Mignerey–Koelsch - Pierre-Thomas Demars- Celeste De Bourmont - Simon Chabanne - Valentin Girard
  - Licence : CC-BY-SA 4.0
  - Résumé : Alors que les discussions sur l’Anthropocène et les limites planétaires sont grandissantes au sein des débats médiatiques, les Technologies de l’Information et de la Communication semblent parfois échapper à la remise en question de leurs impacts. Si la transition numérique est régulièrement présentée comme un allié de la transition écologique, il apparait de plus en plus clairement que le numérique fait plutôt partie du problème que de la solution. À rebours de la transition numérique, il est désormais essentiel d’impulser les idées de sobriété, de low-tech et de démantèlement au sein de ces technologies. Dans ce cadre contextuel, notre équipe de cinq étudiants de l’ENSE3 a été encadrée par le Laboratoire d’informatique de Grenoble et le Low-Tech Lab de Grenoble et aidé par leurs professeurs pour proposer des pistes de réflexion et un prototype pour rediriger le numérique vers les valeurs de la low-tech.
Après avoir fait un état de l’art de la situation du numérique actuel et des initiatives déjà existantes, nous avons décidé de créer un prototype : Dominik. Ce système permet d’avoir accès à des fonctionnalités essentielles du numérique dans des situations où l’accès au numérique traditionnel n’est plus permis, par exemple à cause du manque de ressources matérielles, du manque de disponibilité en énergie, de répression politique, etc... La conception de ce système s’est aussi accompagnée d’une phase de sensibilisation, pour
diffuser l’idée que le monde du numérique n’est pas soutenable sur le long terme, et pour initier les utilisateurs à repenser la place du numérique à travers la prise en main de notre système. Dans une optique de partage de ces connaissances, de diffusion et de réappropriation de notre système, nous avons documenté suivant les principes de la science ouverte la réalisation de Dominik et nous l’avons diffusé sous forme de tutoriel, à la fois sur notre propre site internet éco-conçu, et sur le Wiki du Low-Tech Lab.

### Projet d'intégration des Low-Tech dans les bâtiments collectifs 
  - Emma DEMAREY-WILLIAMS - Romain DUMONT - Suzie MENETRIER - Laurine QUIBBERT - Yannis ROSSET
  - Licence : CC-BY 4.0
  - Grenoble INP - PISTE

### TENUE AUX VAGUES DE CHALEUR : REFROIDISSEMENT SOBRE ET RESPONSABLE DES LOGEMENTS DE LA MÉTROPOLE GRENOBLOISE
  - Théo GALERA - Anna GAY - Gabrielle LEGRAND - Alexandre OMAR - Hugo PHANTHAVONG
  - CC-BY-NC-SA 4.0
